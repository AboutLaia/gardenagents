package labs.laia.gardenagents.Tasks;

import android.os.AsyncTask;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import labs.laia.gardenagents.Activities.HistoryActivity;

/**
 * Created by Laia on 22/06/2016.
 */
public class GetHistoryAsyncTask extends AsyncTask<Integer,Void,ArrayList<HashMap<String, String>>> {
    HistoryActivity parent;
    ArrayList<HashMap<String, String>> data;
    HashMap<String,String> item;

    public void setParent(HistoryActivity parent) {
        this.parent = parent;
    }

    //http://localhost:8080/rest/peticion/historial/{id_robot}/sensor/{id_sensor}
    //{"2016-05-11 20:19:39.0":0,"2016-05-13 03:23:48.0":100}

    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    @Override
    protected ArrayList<HashMap<String, String>> doInBackground(Integer... params) {
        Integer id_maceta=params[0];
        Integer id_sensor=params[1];
        data = new ArrayList<>();
        HttpURLConnection connection = null;

        try {
            URL url = new URL("http://alex-radar.ddns.net:8080/rest/peticion/historial/"+id_maceta+"/sensor/"+id_sensor);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            //Convertir el resultado a string
            String response = convertStreamToString(connection.getInputStream());

            //Separar cada toma de valor
            String[] pairs= response.split(",");

            //Organizar la información
            for (int i=0; i<pairs.length;i++) {
                String[] history = pairs[i].split(":");
                item = new HashMap<>();
                item.put("descripcion",history[0]);
                item.put("valor", history[1]);
                data.add(item);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return data;
    }
}
