package labs.laia.gardenagents.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import labs.laia.gardenagents.Activities.MonitorActivity;

/**
 * Created by Laia on 09/06/2016.
 */
public class PutWaterAsyncTask extends AsyncTask<String,Void,Void> {
    MonitorActivity parent;

    public void setParent(MonitorActivity parent) {
        this.parent = parent;
    }

    protected Void doInBackground(String... params) {
        String value=params[0];
        String id_maceta=params[1];
        String id_actuador=params[2];
        HttpURLConnection connection = null;
        InputStreamReader isr = null;

        try {
            URL url = new URL("http://alex-radar.ddns.net:8080/rest/peticion/actuador/"+id_maceta+"/"+id_actuador+"/"+value);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.getInputStream();
            //connection.setDoOutput(true);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }
}
