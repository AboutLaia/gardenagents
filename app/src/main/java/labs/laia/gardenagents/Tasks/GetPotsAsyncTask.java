package labs.laia.gardenagents.Tasks;

import android.content.SharedPreferences;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import labs.laia.gardenagents.Activities.MainActivity;

/**
 * Created by Laia on 14/06/2016.
 * Tarea asíncrona para saber el número de macetas y mostrarlas en MainActivity
 */
public class GetPotsAsyncTask extends AsyncTask<Void,Void,ArrayList<HashMap<Integer, String>>> {
    MainActivity parent;
    ArrayList<HashMap<Integer, String>> data;
    HashMap<Integer,String> item;
    //http://alex-radar.ddns.net:8080/rest/peticion/macetas

    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public void setParent(MainActivity parent) {
        this.parent = parent;
    }

    @Override
    protected ArrayList<HashMap<Integer, String>> doInBackground(Void... params) {
        data = new ArrayList<>();
        HttpURLConnection connection = null;
        InputStreamReader isr = null;

    try{
        URL url = new URL("http://alex-radar.ddns.net:8080/rest/peticion/macetas");
        connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setDoInput(true);
        if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            isr = new InputStreamReader(connection.getInputStream());
            String response = convertStreamToString(connection.getInputStream());
            JSONObject object = null;

            try {
                object = new JSONObject(response);
                JSONArray list=object.names();
                Integer total=list.length();
                //Recorremos la información recibida para saber quantas macetas hay
                for(int i=0;i<total;i++) {
                    Integer pos=i+1;
                    item = new HashMap<>();
                    item.put(pos,object.getString(pos.toString()));
                    data.add(item);
                }
                //Shared Preferences
                SharedPreferences prefs;
                SharedPreferences.Editor editor;


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    } catch (MalformedURLException e) {
        e.printStackTrace();
    } catch (ProtocolException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }

        return data;
    }

    @Override
    protected void onPostExecute(ArrayList<HashMap<Integer, String>> result) {
        super.onPostExecute(result);
    }
}
