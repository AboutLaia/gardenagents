package labs.laia.gardenagents.Tasks;

import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import labs.laia.gardenagents.Activities.MonitorActivity;

/**
 * Created by Laia on 09/06/2016.
 */
public class PutMovementAsyncTask extends AsyncTask<String,Void,Void> {
    MonitorActivity parent;

    public void setParent(MonitorActivity parent) {
        this.parent = parent;
    }

    protected Void doInBackground(String... params) {
        String sensor1=params[0];
        String valuex=params[1];
        String sensor2=params[2];
        String valuey=params[3];
        String id_maceta=params[4];
        HttpURLConnection connection = null;
        InputStreamReader isr = null;

        try {
            URL url = new URL("http://alex-radar.ddns.net:8080/rest/peticion/actuador/"+id_maceta+"/"+sensor1+"/"+valuex);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.getInputStream();
            //connection.setDoOutput(true);
            connection.disconnect();
            URL url2 = new URL("http://alex-radar.ddns.net:8080/rest/peticion/actuador/"+id_maceta+"/"+sensor2+"/"+valuey);
            connection = (HttpURLConnection) url2.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.getInputStream();
            connection.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

}
