package labs.laia.gardenagents.Tasks;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import labs.laia.gardenagents.Activities.MainActivity;
import labs.laia.gardenagents.Activities.MonitorActivity;
import labs.laia.gardenagents.R;

/**
 * Created by Laia on 09/06/2016.
 *
 * Tarea asíncrona para coger toda la información de una maceta
 */
public class GetInfoAsyncTask extends AsyncTask<Integer,Void,ArrayList<HashMap<String, String>>> {
    //http://alex-radar.ddns.net:8080/rest/peticion/macetas
    MonitorActivity parent;
    ArrayList<HashMap<String, String>> data;
    HashMap<String,String> item;

    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public void setParent(MonitorActivity parent) {
        this.parent = parent;
    }

    @Override
    protected ArrayList<HashMap<String, String>> doInBackground(Integer... params) {
        Integer id_maceta=params[0];
        data = new ArrayList<>();
        HttpURLConnection connection = null;
        InputStreamReader isr = null;

        try{
            URL url = new URL("http://alex-radar.ddns.net:8080/rest/peticion/sensores/"+id_maceta);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                //isr = new InputStreamReader(connection.getInputStream());
                String respons = convertStreamToString(connection.getInputStream());
                //JSONObject object = null;
                String[] part1= respons.split(",");
                String[] temp_aire = part1[0].split(":");
                String[] temp_suelo = part1[1].split(":");
                String[] humedad = part1[2].split(":");
                String[] luz_1 = part1[3].split(":");
                String[] luz_2 = part1[4].split(":");
                String[] ultra_1 = part1[5].split(":");
                String[] ultra_2 = part1[6].split(":");
                String[] nivel_sup = part1[7].split(":");
                String[] nivel_inf = part1[8].split(":");
                //object = new JSONObject(respons);
                int[] IconImages={R.drawable.thermo,R.drawable.thermo,R.drawable.drop,R.drawable.light,R.drawable.light,R.drawable.ultrasounds,R.drawable.ultrasounds,R.drawable.levels,R.drawable.levels};
                item = new HashMap<>();
                item.put("descripcion","Temp. Aire");
                item.put("valor", temp_aire[1]+"ºC");
                item.put("icono",String.valueOf(IconImages[0]));
                data.add(item);
                item=new HashMap<>();
                item.put("descripcion","Temp. Suelo");
                item.put("valor", temp_suelo[1]+"ºC");
                item.put("icono",String.valueOf(IconImages[1]));
                data.add(item);
                item=new HashMap<>();
                item.put("descripcion","Humedad");
                item.put("valor", humedad[1]+"%");
                item.put("icono",String.valueOf(IconImages[2]));
                data.add(item);
                item=new HashMap<>();
                item.put("descripcion","Sensor Luz 1");
                item.put("valor", luz_1[1]+"%");
                item.put("icono",String.valueOf(IconImages[3]));
                data.add(item);
                item=new HashMap<>();
                item.put("descripcion","Sensor Luz 2");
                item.put("valor", luz_2[1]+"%");
                item.put("icono",String.valueOf(IconImages[4]));
                data.add(item);
                item=new HashMap<>();
                item.put("descripcion","S. Ultrasonidos 1");
                item.put("valor", ultra_1[1]+"");
                item.put("icono",String.valueOf(IconImages[5]));
                data.add(item);
                item=new HashMap<>();
                item.put("descripcion","S. Ultrasonidos 1");
                item.put("valor", ultra_2[1]+"");
                item.put("icono",String.valueOf(IconImages[6]));
                data.add(item);
                item=new HashMap<>();
                item.put("descripcion","S. Depósito Sup.");
                item.put("valor", nivel_sup[1]+"");
                item.put("icono",String.valueOf(IconImages[7]));
                data.add(item);
                item=new HashMap<>();
                item.put("descripcion","S. Depósito Inf.");
                item.put("valor", nivel_inf[1]+"");
                item.put("icono",String.valueOf(IconImages[8]));
                data.add(item);



            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    @Override
    protected void onPostExecute(ArrayList<HashMap<String, String>> result) {

        parent.setListPos(result);
        super.onPostExecute(result);
    }

}

