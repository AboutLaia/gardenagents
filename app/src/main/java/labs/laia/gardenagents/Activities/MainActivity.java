package labs.laia.gardenagents.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.HashMap;
import java.util.List;

import labs.laia.gardenagents.R;
import labs.laia.gardenagents.Tasks.GetPotsAsyncTask;

public class MainActivity extends AppCompatActivity {
    SimpleAdapter adapter1;
    ListView potsListView;
    List<HashMap<String, String>> hashMapList;

    ConnectivityManager manager;
    NetworkInfo info;

    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    GetPotsAsyncTask getpots;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Generate the Menu object from the XML resource file
        getMenuInflater().inflate(R.menu.menu_users, menu);

        //Comprobar conectividad
        manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        info = manager.getActiveNetworkInfo();

        //Llamada  a tarea asíncrona
        //Se deja comentado debido a la escazed de datos en la base de datos, se muestran datos de ejemplo
        //getpots = new GetPotsAsyncTask();
        //getpots.execute();


        return super.onCreateOptionsMenu(menu);



    }

    public void mainButtonClicked(View v) {
        Intent intent = null;
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();

        switch (v.getId()) {
            case R.id.bmaceta1:
                editor.putInt("id_maceta",1);
                editor.apply();
                intent = new Intent(this, MonitorActivity.class);
                break;
            case R.id.bmaceta2:
                editor.putInt("id_maceta", 2);
                editor.apply();
                intent = new Intent(this, MonitorActivity.class);
                break;
            case R.id.bmaceta3:
                editor.putInt("id_maceta", 3);
                editor.apply();
                intent = new Intent(this, MonitorActivity.class);
                break;
            case R.id.bmaceta4:
                editor.putInt("id_maceta", 4);
                editor.apply();
                intent = new Intent(this, MonitorActivity.class);
                break;
            case R.id.bmaceta5:
                editor.putInt("id_maceta",5);
                editor.apply();
                intent = new Intent(this, MonitorActivity.class);
                break;

        }

        // Launch the required activity
        if (intent != null) {
            startActivity(intent);
        }
    }


}
