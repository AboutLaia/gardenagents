package labs.laia.gardenagents.Activities;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import labs.laia.gardenagents.R;
import labs.laia.gardenagents.Tasks.GetHistoryAsyncTask;

public class HistoryActivity extends AppCompatActivity {

    //Shared Preferences
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    GetHistoryAsyncTask historytask;
    List<HashMap<String, String>> hashMapList;
    SimpleAdapter adapter1;
    ListView historyListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //editor = prefs.edit();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        Integer id_maceta = prefs.getInt("id_maceta",1);
        Integer id_sensor = prefs.getInt("id_sensor",1);
        Integer[] params={id_maceta,id_sensor};
        historytask = new GetHistoryAsyncTask();
        historytask.execute(params);

        historyListView = (ListView) findViewById(R.id.ActHistory);

        hashMapList = new ArrayList<>();

        hashMapList.addAll(getMockQuotations());
        adapter1 = new SimpleAdapter(this, hashMapList, R.layout.history_list_row,
                new String[]{"descripcion", "valor"}, new int[]{R.id.tvdate, R.id.tvvalue});
        historyListView.setAdapter(adapter1);
    }

    private ArrayList<HashMap<String, String>> getMockQuotations() {
        ArrayList<HashMap<String, String>> result = new ArrayList<>();
        HashMap<String, String> item;
        //int[] IconImages={R.drawable.thermo,R.drawable.thermo,R.drawable.drop,R.drawable.light,R.drawable.light,R.drawable.ultrasounds,R.drawable.ultrasounds,R.drawable.levels,R.drawable.levels};
        //{"2016-05-11 20:19:39.0":0,"2016-05-13 03:23:48.0":100}
        item = new HashMap<>();
        item.put("descripcion", "2016-05-11 13:19:39.0");
        item.put("valor", "28");
        result.add(item);

        item = new HashMap<>();
        item.put("descripcion", "2016-05-11 14:19:39.0");
        item.put("valor", "23");
        result.add(item);

        item = new HashMap<>();
        item.put("descripcion", "2016-05-11 15:19:39.0");
        item.put("valor", "34");
        result.add(item);

        item = new HashMap<>();
        item.put("descripcion", "2016-05-11 16:19:39.0");
        item.put("valor", "76");
        result.add(item);

        item = new HashMap<>();
        item.put("descripcion", "2016-05-11 17:19:39.0");
        item.put("valor", "53");
        result.add(item);

        item = new HashMap<>();
        item.put("descripcion", "2016-05-11 18:19:39.0");
        item.put("valor", "43");
        result.add(item);

        item = new HashMap<>();
        item.put("descripcion", "2016-05-11 19:19:39.0");
        item.put("valor", "34");
        result.add(item);

        item = new HashMap<>();
        item.put("descripcion","2016-05-11 20:19:39.0");
        item.put("valor", "10");
        result.add(item);

        item = new HashMap<>();
        item.put("descripcion","2016-05-11 21:19:39.0");
        item.put("valor", "0");
        result.add(item);

        return result;
    }
}
