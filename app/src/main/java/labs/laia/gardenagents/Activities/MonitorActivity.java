package labs.laia.gardenagents.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Switch;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import labs.laia.gardenagents.Pojo.JoyStickClass;
import labs.laia.gardenagents.R;
import labs.laia.gardenagents.Tasks.GetInfoAsyncTask;
import labs.laia.gardenagents.Tasks.PutMovementAsyncTask;
import labs.laia.gardenagents.Tasks.PutWaterAsyncTask;

public class MonitorActivity extends AppCompatActivity {

    //Tareas asíncronas que se van a ausar
    GetInfoAsyncTask infotask;
    PutWaterAsyncTask watertask;
    PutMovementAsyncTask movetask;

    //Tabhost para construir las pestañas
    TabHost thost;

    // Fuente de datos para mostrar (futuro Async)
    List<HashMap<String, String>> hashMapList;

    //List Views para mostrar info
    ListView sensorsListView;

    //Adapter para mostrar la info
    SimpleAdapter adapter1;

    //joystick
    JoyStickClass js;

    //Tv para mostrar la dirección del joystick
    TextView tvDirection;

    //Variables para comprobar conexión a internet
    ConnectivityManager manager;
    NetworkInfo info;

    //Shared Preferences
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    Button stopbutton;



    public void setListPos(ArrayList<HashMap<String, String>> result){
        hashMapList.addAll(result);
        if(hashMapList!=null) {
            //Adapter para enlazar la información con la interfaz
            adapter1 = new SimpleAdapter(this, hashMapList, R.layout.monitor_list_row,
                    new String[]{"icono", "descripcion", "valor"},
                    new int[]{R.id.icon_monitor, R.id.tvtipo, R.id.tvvalor});
            sensorsListView.setAdapter(adapter1);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitor);
        sensorsListView = (ListView) findViewById(R.id.tabSensors);

        //Preferencias
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        editor = prefs.edit();
        final String id_maceta= Integer.toString(prefs.getInt("id_maceta", 1));

        tvDirection = (TextView) findViewById(R.id.tvDirection);
        stopbutton = (Button) findViewById(R.id.stop_button);

        // Creación de los TabHost
        thost = (TabHost) findViewById(R.id.tabHost);
        thost.setup();
        TabHost.TabSpec spec = thost.newTabSpec(getString(R.string.tab_sensors));

        //Pestaña Sensores
        spec.setContent(R.id.tabSensors);
        spec.setIndicator("Sensores");
        thost.addTab(spec);

        //Pestaña Actuadores
        spec = thost.newTabSpec(getString(R.string.tab_actuadores));
        spec.setIndicator("Actuadores");
        spec.setContent(R.id.tabActuators);
        thost.addTab(spec);


        //Pestaña Motor
        spec = thost.newTabSpec(getString(R.string.tab_motor));
        spec.setIndicator("Motor");
        spec.setContent(R.id.tabMotor);
        thost.addTab(spec);

        //Ancho de las pestañas
        thost.getTabWidget().getChildAt(0).getLayoutParams().width = 10;
        thost.getTabWidget().getChildAt(1).getLayoutParams().width = 80;
        thost.getTabWidget().getChildAt(2).getLayoutParams().width = 10;

        manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        info = manager.getActiveNetworkInfo();

        if ((info != null) && info.isConnected()) {
            movetask = new PutMovementAsyncTask();
            movetask.setParent(MonitorActivity.this);
        }

        //Switch de activacion de riego
        final Switch waterswitch = (Switch) findViewById(R.id.switch_water);
        Integer v=prefs.getInt("bomba_agua",10);

        //Si el riego esta activado, marcar el switch omo activado
        if (v==100){
            waterswitch.setChecked(true);
        }

        //ACTIVAR/DESACTIVAR RIEGO
        //Ejecutar tarea de activacion o desactivacion de riego cuando se pulse el boton
        waterswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                watertask = new PutWaterAsyncTask();
                watertask.setParent(MonitorActivity.this);
                //isChecked será true para la activación del riego
                if (isChecked) {
                    String[] params = {"100", id_maceta, "2"};
                    watertask.execute(params);
                    editor.putInt("bomba_agua", 0);
                    editor.apply();

                } else {
                    String[] params = {"10", id_maceta, "2"};
                    watertask.execute(params);
                    editor.putInt("bomba_agua", 1);
                    editor.apply();

                }
                Log.v("Switch State=", "" + isChecked);
            }

        });


        //INFO SENSORES
        // Llamada a tarea asíncrona de obtencion de informacion de sensores
        hashMapList = new ArrayList<>();
        infotask = new GetInfoAsyncTask();
        infotask.setParent(MonitorActivity.this);
        Integer id_maceta2 = prefs.getInt("id_maceta",1);
        infotask.execute(id_maceta2);


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Proceso alternativo para utilizar cuando el servidor no está en funcionamiento
        hashMapList.addAll(getMockQuotations());
        adapter1 = new SimpleAdapter(this, hashMapList, R.layout.monitor_list_row,
                    new String[]{"icono", "descripcion", "valor"}, new int[]{R.id.icon_monitor, R.id.tvtipo, R.id.tvvalor});
        sensorsListView.setAdapter(adapter1);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////



        //PART DEL MOTOR(JOYSTICK)
        RelativeLayout layout_joystick;
        layout_joystick = (RelativeLayout)findViewById(R.id.layout_joystick);

        js = new JoyStickClass(getApplicationContext(), layout_joystick, R.drawable.image_button);

        js.setStickSize(200, 200);
        js.setLayoutSize(500, 500);
        js.setStickAlpha(100);
        js.setOffset(90);
        js.setMinimumDistance(80);


        layout_joystick.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                js.drawStick(arg1);
                if (arg1.getAction() == MotionEvent.ACTION_DOWN
                        || arg1.getAction() == MotionEvent.ACTION_MOVE) {
                    //textView1.setText("X : " + String.valueOf(js.getX()));
                    //textView2.setText("Y : " + String.valueOf(js.getY()));
                    //textView3.setText("Angle : " + String.valueOf(js.getAngle()));
                    //textView4.setText("Distance : " + String.valueOf(js.getDistance()));

                    int direction = js.get8Direction();
                    if (direction == JoyStickClass.STICK_UP) {
                        tvDirection.setText("Dirección : Avanza");
                        if ((info != null) && info.isConnected()) {
                            movetask = new PutMovementAsyncTask();
                            movetask.setParent(MonitorActivity.this);
                            String[] params = {"6", "0", "7", "100", id_maceta};
                            movetask.execute(params);
                            Log.i("info de movimeent", params[0]);
                        }
                        //} else if (direction == JoyStickClass.STICK_UPRIGHT) {
                        //tvDirection.setText("Direction : Up Right");
                    } else if (direction == JoyStickClass.STICK_RIGHT) {
                        tvDirection.setText("Dirección : Derecha");
                        if ((info != null) && info.isConnected()) {
                            movetask = new PutMovementAsyncTask();
                            movetask.setParent(MonitorActivity.this);
                            String[] params = {"6", "100", "7", "0", id_maceta};
                            movetask.execute(params);
                        }

                        //} else if (direction == JoyStickClass.STICK_DOWNRIGHT) {
                        //tvDirection.setText("Direction : Down Right");
                    } else if (direction == JoyStickClass.STICK_DOWN) {
                        tvDirection.setText("Dirección : Retrocede");
                        if ((info != null) && info.isConnected()) {
                            movetask = new PutMovementAsyncTask();
                            movetask.setParent(MonitorActivity.this);
                            String[] params = {"6", "0", "7", "-100", id_maceta};

                            movetask.execute(params);
                        }

                        //} else if (direction == JoyStickClass.STICK_DOWNLEFT) {
                        //tvDirection.setText("Direction : Down Left");
                    } else if (direction == JoyStickClass.STICK_LEFT) {
                        tvDirection.setText("Dirección : Izquierda");
                        if ((info != null) && info.isConnected()) {
                            movetask = new PutMovementAsyncTask();
                            movetask.setParent(MonitorActivity.this);
                            String[] params = {"6", "-100", "7", "0", id_maceta};
                            movetask.execute(params);
                        }

                        //} else if (direction == JoyStickClass.STICK_UPLEFT) {
                        //tvDirection.setText("Direction : Up Left");
                        //} else if (direction == JoyStickClass.STICK_NONE) {
                        //tvDirection.setText("Direction : Center");
                    }
                } else if (arg1.getAction() == MotionEvent.ACTION_UP) {
                    //textView1.setText("X :");
                    //textView2.setText("Y :");
                    //textView3.setText("Angle :");
                    //textView4.setText("Distance :");
                    tvDirection.setText("Dirección :");
                    if ((info != null) && info.isConnected()) {
                        movetask = new PutMovementAsyncTask();
                        movetask.setParent(MonitorActivity.this);
                        String[] params = {"6", "0", "7", "0", id_maceta};
                        movetask.execute(params);
                    }
                }
                return true;
            }
        });

        //Configuracion para que, al pulsar el boton stop,
        //se haga una llamada a la tarea asincrona encargada
        //de enviar la detencion de la maceta
        stopbutton.setOnClickListener(listener);

        //Método para acceder al historial según el elemento pulsado
        sensorsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                editor = prefs.edit();
                switch (position) {
                    case 1:
                        editor.putInt("id_sensor", 1);
                        editor.apply();
                        break;
                    case 2:
                        editor.putInt("id_sensor", 2);
                        editor.apply();
                        break;
                    case 3:
                        editor.putInt("id_sensor", 3);
                        editor.apply();
                        break;
                    case 4:
                        editor.putInt("id_sensor", 4);
                        editor.apply();
                        break;
                    case 5:
                        editor.putInt("id_sensor", 5);
                        editor.apply();
                        break;
                    case 6:
                        editor.putInt("id_sensor", 6);
                        editor.apply();
                        break;
                    case 7:
                        editor.putInt("id_sensor", 7);
                        editor.apply();
                        break;
                    case 8:
                        editor.putInt("id_sensor", 8);
                        editor.apply();
                        break;
                    case 9:
                        editor.putInt("id_sensor", 9);
                        editor.apply();
                        break;

                }
                Intent intent = null;
                intent = new Intent(MonitorActivity.this, HistoryActivity.class);
                startActivity(intent);

            }

        });
    }
                //Metodo que llama el botón de refresco

    public void refresh(View view){
        onRestart();
    }

    View.OnClickListener listener = new View.OnClickListener() {
        public void onClick(View v) {
            String id_maceta= Integer.toString(prefs.getInt("id_maceta", 1));
            //Llamada a tarea asincrona para detener maceta
            if ((info != null) && info.isConnected()) {
                movetask = new PutMovementAsyncTask();
                movetask.setParent(MonitorActivity.this);
                String[] params = {"6", "0", "7", "0", id_maceta};
                movetask.execute(params);
            }
        }
    };
    @Override
    protected void onRestart() {

        // Metodo para refrescar la página
        super.onRestart();
        Intent i = new Intent(this, MonitorActivity.class);  //your class
        startActivity(i);
        finish();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Generate the Menu object from the XML resource file
        getMenuInflater().inflate(R.menu.menu_refresh, menu);

        //Comprobar conectividad
        manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        info = manager.getActiveNetworkInfo();

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                infotask = new GetInfoAsyncTask();
                infotask.setParent(MonitorActivity.this);
                Integer id_maceta2 = prefs.getInt("id_maceta",1);
                infotask.execute(id_maceta2);
                //Recargar la página
                refresh(sensorsListView);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    private ArrayList<HashMap<String, String>> getMockQuotations() {
        ArrayList<HashMap<String, String>> result = new ArrayList<>();
        HashMap<String, String> item;
        int[] IconImages={R.drawable.thermo,R.drawable.thermo,R.drawable.drop,R.drawable.light,R.drawable.light,R.drawable.ultrasounds,R.drawable.ultrasounds,R.drawable.levels,R.drawable.levels};

        item = new HashMap<>();
        item.put("descripcion","Temp. Aire");
        item.put("valor", "28");
        item.put("icono", String.valueOf(IconImages[0]));
        result.add(item);

        item = new HashMap<>();
        item.put("descripcion","Temp. Suelo");
        item.put("valor", "23");
        item.put("icono", String.valueOf(IconImages[1]));
        result.add(item);

        item = new HashMap<>();
        item.put("descripcion","Humedad");
        item.put("valor", "34");
        item.put("icono", String.valueOf(IconImages[2]));
        result.add(item);

        item = new HashMap<>();
        item.put("descripcion","Sensor Luz 1");
        item.put("valor", "76");
        item.put("icono", String.valueOf(IconImages[3]));
        result.add(item);

        item = new HashMap<>();
        item.put("descripcion","Sensor Luz 2");
        item.put("valor", "53");
        item.put("icono", String.valueOf(IconImages[4]));
        result.add(item);

        item = new HashMap<>();
        item.put("descripcion","S. Ultrasonidos 1");
        item.put("valor", "3");
        item.put("icono", String.valueOf(IconImages[5]));
        result.add(item);

        item = new HashMap<>();
        item.put("descripcion","S. Ultrasonidos 2");
        item.put("valor", "4");
        item.put("icono", String.valueOf(IconImages[6]));
        result.add(item);

        item = new HashMap<>();
        item.put("descripcion","S. Depósito Sup");
        item.put("valor", "100");
        item.put("icono", String.valueOf(IconImages[7]));
        result.add(item);

        item = new HashMap<>();
        item.put("descripcion","S. Depósito Inf");
        item.put("valor", "0");
        item.put("icono", String.valueOf(IconImages[8]));
        result.add(item);

        return result;
    }

}
